# Von-Neumann vs Hardvard
```plantuml
@startmindmap
*[#lightblue] Von-Neumann vs Hardvard
	*[#lightblue] Tanto Neumann como Harvard\nrealizaron grandes aportes a\nlo que hoy en día conocemos\nsobre la arquitectura de\ncomputadoras.
		*[#Pink] ¿Quién es Von-Neumann?
			*[#Yellow] Historia
				*[#Orange] Nació el 28 de diciembre de 1903 en Budapest/Hungria
				*[#Orange] Murió el 8 de febrero de 1957 en Washington D.C/USA
			*[#Yellow] Aportaciones a\nlas matematicas
				*[#Orange] Doctorado de matemáticas a los 23 años
				*[#Orange] Número ordinal
				*[#Orange] Teoria de conjuntos.
			*[#Yellow] Aportaciones a la\ncomputación
				*[#Orange] Obtencion de bit de paridad
				*[#Orange] Diseño logico
				*[#Orange] Diseño del primer ordenador
				*[#Orange] Separar el hardware del software y sus restricciones
				*[#Orange] Diseño actual de las computadoras
					*[#lightblue] Unidad de Control
						*[#lightgreen] Encargada de las etapas de captación y descodificación del ciclo de instrucción
					*[#lightblue] Unidad logico-aritmética o ALU
						*[#lightgreen] Encargada de realizar las operaciones matemáticas y de lógica que requieren los programas
					*[#lightblue] Memoria
						*[#lightgreen] La memoria en la que se almacena el programa, la cual la conocemos como memoria RAM
					*[#lightblue] Dispositivo de entrada
						*[#lightgreen] Desde el que nos comunicamos con el ordenador.
					*[#lightblue] Dispositivo de Salida
						*[#lightgreen] Desde el que el ordenador se comunica con nosotros.
				*[#Orange] Programa de almacenado en memoria de los datos
		*[#Pink] Modelos de arquirtectura
			*[#Yellow] La organización del computador según\nel modelo Harvard, básicamente, se\ndistingue del modelo Von Neumann por\nla división de la memoria en una memoria\nde instrucciones y una memoria de datos,\nde manera que el procesador puede\nacceder separada y simultáneamente a las dos\nmemorias
				*[#Orange] Modelo de Hardvard
					*[#lightgreen] Es una arquitectura de computadora con pistas de almacenamiento y de señal\nfísicamente separadas para las instrucciones y para los datos
					*[#lightgreen] Las primeras máquinas tenían almacenamiento de datos totalmente contenido\ndentro la unidad central de proceso
					*[#lightgreen] Arquitectura con memoria de programa y de datos separadas y solo\n accesibles a través de buses distintos
						*[#Pink] Ventajas
							*[#Orange] El tamaño de las instrucciones no está relacionado con el de los datos
							*[#Orange] El tiempo de acceso a las instrucciones puede superponerse con el de los datos
						*[#Pink] Desventajas
							*[#Orange] Deben poseer instrucciones especiales para acceder a tablas de\nvalores constantes que pueda ser necesario incluir en los programas
				*[#Orange] Modelo de Neumann
					*[#lightblue] Tenía la visión de crear un modelo estándar\nque funcionase en cualquier ámbito o aplicación
					*[#lightblue] Separar el software del hardware
					*[#lightblue] Crear un modelo de computador universal				
@endmindmap
```
# Supercomputadoras en México
```plantuml
@startmindmap
*[#lightblue] Supercomputadoras en México
	*[#Pink] ¿Qué son?
		*[#Yellow] Ordenadores de alto desempeño que son extremadamente potentes.
	*[#Pink] ¿Para qué sirven?
		*[#Yellow] Aparte de las grandes aportaciones que cada\nuna de las mencionadas en este mapa han hecho\nindividualmente se usan para poder realizar\ncalculos mucho mas rápido de lo habitual y\nprocear información de igual manera.
	*[#Pink] Historia
		*[#Yellow] En el año de 1958 la UNAM obtuvo la IBM-650\nla que fue la primera supercomputadora en\nlatinoamerica, la cual funcionaba con bulbos y se uso\npara realizar investigación cientifica pero usaba\nrecursos en exceso como lo es el espacio\n y la energia.
	*[#Pink] ¿Cuáles son?
		*[#Yellow] KANBALAM
			*[#lightgreen] Año 2007
			*[#lightgreen] Tiene la capacidad de realizar cantidades enormes de opercaciones por segundo
			*[#lightgreen] Podia apurar tareas en semanas para las cuales se necesitarian al menos 25 años
			*[#lightgreen] 7 billones de operaciones matematicas por segundo
		*[#Yellow] MIZTLI
			*[#lightgreen] Año 2012
			*[#lightgreen] Adquirida por la UNAM
			*[#lightgreen] Cuenta con la capacidad de 8344 procesadores
		*[#Yellow] XIUHCOATL
			*[#lightgreen] Año 2012
			*[#lightgreen] Fue un proyecto elaborado por la UNAM, UAM y el IPN
			*[#lightgreen] Contaba con las caracteristicas de tener un\nnodo robusto de supercomputadora.
		*[#Yellow] CRAY 432
			*[#lightgreen] Año 1991
			*[#lightgreen] Fue la primera super computadora en America latina
			*[#lightgreen] Tiene la equivalencia de 2000 computadoras normales
		*[#Yellow] BUAP
			*[#lightgreen] Se usa para realizar investigaciones cientificas
				*[#lightblue] Astronomia
				*[#lightblue] Quimica
				*[#lightblue] Aeronautica
			*[#lightgreen] Es una de las cinco mas poderosas de america latina
	*[#Pink] Centro de supercomputación
		*[#Yellow] Centro que obtuvo una de las computadoras mas potentes de de America latina.
@endmindmap